<?php

/* ##Exercice 5

Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M". Les afficher comme ceci :

Nom : Nom du client

Prénom : Prénom du client

Trier les noms par ordre alphabétique. 


SELECT lastName, firstName FROM clients WHERE lastName LIKE 'M%'' ORDER BY lastName */



$bdd = new PDO('mysql:host=localhost;dbname=colyseum', 'chris974', 'phpmyadmin974');
$reponse = $bdd->query('SELECT lastName, firstName FROM clients WHERE lastName LIKE "M%" ORDER BY lastName');
while ($donnees = $reponse->fetch())
{
  echo '<p>' . "Nom : " . $donnees['lastName'] . '<p>' ;
  echo '<p>' . "Prénom : " . $donnees['firstName'] . '</p>' ;
}